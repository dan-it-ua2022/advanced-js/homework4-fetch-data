function renderMovie(movie) {
  console.log(movie);
  const div = document.createElement('div');
  const movieName = document.createElement('span');
  movieName.innerText = movie.name;
  const chars = document.createElement('div');
  div.append(movieName);
  div.append(chars);

  document.body.append(div);

  const characterRequest = movie.characters.map((charUrl) =>
    fetch(charUrl).then((r) => r.json())
  );

  Promise.all(characterRequest).then((characters) => {
    console.log(characters);

    characters.forEach((character) => {
      const c = document.createElement('span');
      c.innerText = character.name;
      chars.append(c);
    });
  });
}

fetch('https://ajax.test-danit.com/api/swapi/films')
  .then((r) => r.json())
  .then((data) => data.forEach((movie) => renderMovie(movie)));
